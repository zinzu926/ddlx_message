/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.messages;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wwzz.messages.utils.Constants;
import com.wwzz.services.api.MessageServices;
import com.wwzz.services.domain.booking.TUserBookingOrder;
import com.wwzz.services.domain.booking.TUserBookingTempOrder;
import com.wwzz.services.domain.booking.TUserOrderDocents;
import com.wwzz.services.domain.journey.TUserJourney;
import com.wwzz.services.domain.news.TUserNotice;
import com.wwzz.services.domain.users.TUser;
import com.wwzz.services.domain.users.TUserIdentityLongGuide;
import com.wwzz.services.domain.users.TUserIdentityOGuide;
import com.wwzz.services.repository.TUserBookingTempOrderRepository;
import com.wwzz.services.repository.TUserIdentityLongGuideRepository;
import com.wwzz.services.repository.TUserIdentityOGuideRepository;
import com.wwzz.services.repository.TUserJourneyRepository;
import com.wwzz.services.repository.TUserNoticeRepository;
import com.wwzz.services.repository.TUserOrderDocentsRepository;
import static com.wwzz.services.utils.Constants.BOOKING_CATEGORIES;
import static com.wwzz.services.utils.Constants.BOOKING_CATEGORY_DOCENTS;
import static com.wwzz.services.utils.Constants.BOOKING_CATEGORY_LC;
import static com.wwzz.services.utils.Constants.BOOKING_CATEGORY_LG;
import static com.wwzz.services.utils.Constants.BOOKING_CATEGORY_OG;
import static com.wwzz.services.utils.Constants.BOOKING_CATEGORY_PP;
import com.wwzz.services.utils.Utils;
import com.wwzz.weixin.AccessToken;
import com.wwzz.weixin.Template;
import com.wwzz.weixin.Template2;
import com.wwzz.weixin.utils.WeixinUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import javapns.devices.Device;
import javapns.devices.exceptions.InvalidDeviceTokenFormatException;
import javapns.devices.implementations.basic.BasicDevice;
import javapns.notification.PushNotificationPayload;
import org.apache.commons.lang3.math.NumberUtils;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

/**
 *
 * @author JBOSS
 */
@Transactional
public class MessageProcess implements MessageServices,Constants{
    @Autowired
    private TUserNoticeRepository userNRepo;
    @Autowired
    @Qualifier("pushService")
    private PushNotification pushService;
    @Autowired
    @Qualifier("messageHelper")
    private MessageHelper messageHelper;
    @Autowired
    private TUserBookingTempOrderRepository tempRepo;
    @Autowired
    private MessageSource resources;
    @Autowired
    private TUserJourneyRepository jrRepo;
    @Autowired
    private TUserIdentityLongGuideRepository lgRepo;
    @Autowired
    private TUserIdentityOGuideRepository ogRepo;
    @Autowired
    private TUserOrderDocentsRepository docRepo;
    
    @Override
    public void process_booking_message(String notice) {
        TUserNotice nt=userNRepo.findByMessage(notice);
        if(nt==null)return;
        List<TUser> users=nt.getRecipients();
        TUserBookingOrder bo=nt.getBooking();
        String msgs=messageHelper.createMqttMessage(nt,bo.getId().toString(),bo.getCategory());
        List<String> mts=new ArrayList<>();
        for (TUser user : users) {
            if(hasText(user.getOpenid())){
                AccessToken act=WeixinUtil.getAccessToken(WEIXIN_APP_ID, WEIXIN_APP_SEC);
                if(act!=null){
                    Template tmp=messageHelper.createWeixinBookingTemplate(nt.getBooking(), user.getOpenid(), 2,nt.getId().toString());
                    if(tmp!=null)WeixinUtil.sendTemplateNews(tmp, act.getToken());
                }
            }
            if(hasText(user.getTopicid()))mts.add(user.getTopicid());
        }
        if(!mts.isEmpty()){
            pushService.send_mqtt_topic(mts,msgs);
        }
        pushService.send_apple_push_message(messageHelper.createAppleMessage(users, nt));
        
    }

    @Override
    public boolean hasText(String s) {
        return StringUtils.hasText(s);
    }
    /**
     * 
     * @param notice
     * @param bbid 
     */
    @Override
    public void journey_state_notice(String notice,String bbid) {
        TUserNotice nt=userNRepo.findByMessage(notice);
        if(nt==null)return;
        TUserJourney ju=jrRepo.findOne(Long.parseLong(bbid));
        if(ju==null)return;
        List<TUser> users=nt.getRecipients();
        List<String> topics = new ArrayList<>();
        List<Device> app_messages = new ArrayList();
        String s,msg=null;
        Map m = new HashMap();
        m.put("msgid", UUID.randomUUID().toString());
        m.put("title", nt.getTitle());
        m.put("cat", nt.getSubtype());
        m.put("bbid", bbid);
        m.put("nid", nt.getId().toString());
        ObjectMapper om = new ObjectMapper();
        try {
            msg=messageHelper.securityChar() + om.writeValueAsString(m) + messageHelper.securityChar();
        } catch (JsonProcessingException ex) {}
        for (TUser user : users) {
            if (hasText(user.getTopicid())) {
                topics.add(user.getTopicid());
            }
            if(hasText(user.getOpenid())){
               s=resources.getMessage("notice.journey",new Integer[]{ju.getCountryNumber(),ju.getDayNumber()},
                            new Locale("zh","CN"));
               AccessToken act=WeixinUtil.getAccessToken(WEIXIN_APP_ID, WEIXIN_APP_SEC);
               if(act!=null){
                  Template tmp=messageHelper.createWeixinJourneyNotice(ju.getNameZh(),nt.getSubtype(),bbid, user.getOpenid(), nt.getId().toString());
                  if(tmp!=null)WeixinUtil.sendTemplateNews(tmp, act.getToken());  
               }
            }
            //
            if (hasText(user.getDeviceid())) {
                try {
                    BasicDevice device = new BasicDevice(user.getDeviceid());
                    app_messages.add(device);
                } catch (InvalidDeviceTokenFormatException ex) {}
            }
            
        }
        if(!topics.isEmpty())pushService.send_mqtt_topic(topics,msg);
        PushNotificationPayload py = new PushNotificationPayload();
        try {
            py.addCustomDictionary("title", trim(nt.getTitle()));
            py.addCustomDictionary("cat", nt.getSubtype());
            py.addCustomDictionary("bbid", bbid);
            py.addCustomDictionary("nid", nt.getId().toString());
            py.addAlert(trim(nt.getTitle()));
            py.addSound("default");
            py.addBadge(1);
            Map m1 = new HashMap();
            m1.put("payload", py);
            m1.put("devices", app_messages);
            pushService.send_apple_push_message(m1);
        } catch (JSONException ex) {}
        
    }

    @Override
    public void booking_cancel_notice(String notice) {
        TUserNotice nt=userNRepo.findByMessage(notice);
        if(nt==null)return;
        TUserBookingOrder bk=nt.getBooking();
        if(bk==null)return;
        List<TUser> users=nt.getRecipients();
        List<String> topics=new ArrayList<>();
        if (users!=null) {
            for (TUser user : users) {
                if(hasText(user.getOpenid())){
                   AccessToken act=WeixinUtil.getAccessToken(WEIXIN_APP_ID, WEIXIN_APP_SEC);
                   Float price;
                   if(act!=null){
                      if(bk.getCategory()<=BOOKING_CATEGORY_LC)
                           price=bk.getExpectedAmount();
                       else price=bk.getOrderAmount();
                      Template tmp=messageHelper.createWeixinBookingCancelNotice(100,nt.getTitle(), bk.getSerialNumber(), Utils.priceToString(price)+"", "" ,user.getOpenid(), nt.getId().toString());
                      if(tmp!=null)WeixinUtil.sendTemplateNews(tmp, act.getToken());  
                   }
                }
                if(hasText(user.getTopicid()))topics.add(user.getTopicid());
            }
        }
        pushService.send_mqtt_topic(topics, messageHelper.mqtt_message_by_booking_cancel(nt, bk.getId().toString()));
        pushService.send_apple_push_message(messageHelper.apple_message_by_book_cancel(users, nt, bk.getId().toString()));
    }

    @Override
    public void user_skill_notice(String notice) {
        TUserNotice nt=userNRepo.findByMessage(notice);
        if(nt==null)return;
        List<TUser> users=nt.getRecipients();
        int ccat=nt.getSubtype();
        List<String> topics=new ArrayList<>();
        List<Device> app_messages=new ArrayList();
        Map m=new HashMap();
        m.put("msgid", UUID.randomUUID().toString());
        m.put("title", nt.getTitle());
        m.put("nid", nt.getId().toString());
        m.put("cat", ccat);
        ObjectMapper om=new ObjectMapper();
        String msg="";
        try {
            msg=messageHelper.securityChar()+om.writeValueAsString(m)+messageHelper.securityChar();
        } catch (JsonProcessingException ex) {}
        for(TUser user:users){
            if(hasText(user.getTopicid())){
                topics.add(user.getTopicid());
            }
            if(hasText(user.getOpenid())){
               AccessToken act=WeixinUtil.getAccessToken(WEIXIN_APP_ID, WEIXIN_APP_SEC);
               if(act!=null){
                  Template tmp=messageHelper.createWeixinSkillNotice(nt.getSubtype(),user.getName(),"--",user.getMobile(),user.getOpenid(), nt.getId().toString());
                  if(tmp!=null)WeixinUtil.sendTemplateNews(tmp, act.getToken());  
               }
            }
            if(hasText(user.getDeviceid())){
                try {
                    BasicDevice device=new BasicDevice(user.getDeviceid());
                    app_messages.add(device);
                } catch (InvalidDeviceTokenFormatException ex) {
                }
            }
        }
        if(!topics.isEmpty())pushService.send_mqtt_topic(topics,msg);
        try {
            PushNotificationPayload py = new PushNotificationPayload();
            py.addCustomDictionary("title", trim(nt.getTitle()));
            py.addCustomDictionary("cat", ccat);
            py.addCustomDictionary("nid", nt.getId().toString());
            py.addAlert(trim(nt.getTitle()));
            py.addSound("default");
            py.addBadge(1);
            Map m1 = new HashMap();
            m1.put("payload", py);
            m1.put("devices", app_messages);
            pushService.send_apple_push_message(m1);
        } catch (JSONException ex) {
        }
        
    }
    private String trim(String s){
        if(s==null)return null;
        if(s.length()>20)return s.substring(0, 15)+"...";
        else return s;
    }
    @Override
    public void bid_notice(String bbid) {
        if(!hasText(bbid) || !NumberUtils.isNumber(bbid))return;
        TUserBookingTempOrder tb=tempRepo.findOne(Long.parseLong(bbid));
        AccessToken act=WeixinUtil.getAccessToken(WEIXIN_APP_ID, WEIXIN_APP_SEC);
        if(act==null)return;
        TUser user =tb.getBooking().getApplicant();
        if(user.getReceivingTaskNotice()!=1)return;
        if(hasText(user.getOpenid())){
            Template tmp=messageHelper.createWeixinBookingTemplate(tb, user.getOpenid(), 1,"");
            if(tmp!=null)WeixinUtil.sendTemplateNews(tmp, act.getToken());
        }
        List<TUser> users=new ArrayList();
        List<String> topics=new ArrayList();
        users.add(user);
        if(hasText(user.getDeviceid())){
            pushService.send_apple_push_message(messageHelper.createAppleBidMessage(users, tb));
        }
        if(hasText(user.getTopicid())){
            topics.add(user.getTopicid());
            String msgs=messageHelper.createBidMqttMessage(tb);
            if(!topics.isEmpty()){
                pushService.send_mqtt_topic(topics, msgs);
            }
        }
    }

    @Override
    public void notice_cancel_no_bidder_booking(String data) {
        if(!hasText(data))return;
        List<Device> app_messages=new ArrayList();
        String[] ids=StringUtils.split(data, "/");
        for (int i = 0; i < ids.length; i++) {
            String oo = ids[i];
            TUserNotice notice=userNRepo.findByMessage(oo);
            if(notice==null)continue;
            List<TUser> users=notice.getRecipients();
            List<String> messages=new ArrayList<>();
            List<String> topics=new ArrayList<>();
            for (TUser user : users) {
                if(hasText(user.getTopicid())){
                    Map m=new HashMap();
                    m.put("msgid", UUID.randomUUID().toString());
                    m.put("title", notice.getTitle());
                    m.put("nid", notice.getId().toString());
                    m.put("cat", notice.getSubtype());
                    ObjectMapper om=new ObjectMapper();
                    try {
                        messages.add(messageHelper.securityChar()+om.writeValueAsString(m)+messageHelper.securityChar());
                        topics.add(user.getTopicid());
                    } catch (JsonProcessingException ex) {}
                }
                if(hasText(user.getOpenid())){
                   Float price;
                   AccessToken act=WeixinUtil.getAccessToken(WEIXIN_APP_ID, WEIXIN_APP_SEC);
                   if(act!=null){
                       if(notice.getBooking().getCategory()<=BOOKING_CATEGORY_LC)
                           price=notice.getBooking().getExpectedAmount();
                       else price=notice.getBooking().getOrderAmount();
                      Template tmp=messageHelper.createWeixinBookingCancelNotice(2,notice.getTitle(), com.wwzz.services.utils.Constants.BOOKING_CATEGORIES[notice.getBooking().getCategory()-1], Utils.priceToString(price)+"", 
                                            Utils.dateToString(notice.getBooking().getServiceTime()), user.getOpenid(), notice.getId().toString());
                      if(tmp!=null)WeixinUtil.sendTemplateNews(tmp, act.getToken());  
                   }
                }
                if(hasText(user.getDeviceid())){
                    try {
                        BasicDevice device=new BasicDevice(user.getDeviceid());
                        app_messages.add(device);
                    } catch (InvalidDeviceTokenFormatException ex) {
                    }
                }
            }
            pushService.send_mqtt_topic(topics,messages);
            try {
                PushNotificationPayload py = new PushNotificationPayload();
                py.addCustomDictionary("title", trim(notice.getTitle()));
                py.addCustomDictionary("cat", notice.getSubtype());
                py.addCustomDictionary("nid", notice.getId().toString());
                py.addAlert(trim(notice.getTitle()));
                py.addSound("default");
                py.addBadge(1);
                Map m = new HashMap();
                m.put("payload", py);
                m.put("devices", app_messages);
                pushService.send_apple_push_message(m);
            } catch (JSONException ex) {}
            
        }
    }
    /**
     * 
     * @param data 
     */
    @Override
    public void notice_booking_pay_state(String data) {
        if(!hasText(data))return;
        String[] ids=StringUtils.split(data, "/");
        for (int i = 0; i < ids.length; i++) {
            String oo = ids[i];
            TUserNotice notice=userNRepo.findByMessage(oo);
            if(notice==null)continue;
            List<TUser> users=notice.getRecipients();
            List<String> messages=new ArrayList<>();
            List<String> topics=new ArrayList<>();
            List<Device> app_messages=new ArrayList();
            String lsu="400";
            if(notice.getBooking().getCategory()==BOOKING_CATEGORY_PP){
                lsu="300";
            }else if(notice.getBooking().getCategory()==BOOKING_CATEGORY_DOCENTS){
                TUserOrderDocents dc=docRepo.findByBooking(notice.getBooking());
                if(dc!=null){
                    if(dc.getType()==2)lsu="300";
                    else lsu="400";
                }
            }else{
                lsu="400";
            }
            for (TUser user : users) {
                if(hasText(user.getTopicid())){
                    Map m=new HashMap();
                    m.put("msgid", UUID.randomUUID().toString());
                    m.put("title", notice.getTitle());
                    m.put("nid", notice.getId().toString());
                    m.put("cat", notice.getSubtype());
                    m.put("bid", notice.getBooking().getId().toString());
                    m.put("status", lsu);
                    ObjectMapper om=new ObjectMapper();
                    try {
                        messages.add(messageHelper.securityChar()+om.writeValueAsString(m)+messageHelper.securityChar());
                        topics.add(user.getTopicid());
                    } catch (JsonProcessingException ex) {}
                }
                if(hasText(user.getOpenid())){
                   AccessToken act=WeixinUtil.getAccessToken(WEIXIN_APP_ID, WEIXIN_APP_SEC);
                   if(act!=null){
                       String company;
                       String bbid;
                       if(notice.getBooking().getCategory()==BOOKING_CATEGORY_LG){
                           TUserIdentityLongGuide lg=lgRepo.findByUserAndStatus(user, 1);
                           if(lg!=null){
                               if(lg.getTravelAgency()!=null)company=lg.getTravelAgency().getNameZh();
                               else company="--";
                           }else company="--";
                       }else if(notice.getBooking().getCategory()==BOOKING_CATEGORY_OG){
                           TUserIdentityOGuide og=ogRepo.findByUserAndStatus(user, 1);
                           if(og!=null){
                               if(og.getTravelAgency()!=null)company=og.getTravelAgency().getNameZh();
                               else company="--";
                           }else company="--";
                       }else company="--";
                       bbid=notice.getBooking().getId().toString();
                      Template2 tmp=messageHelper.createWeixinPayReadyNotice(notice.getSubtype(),company, 
                              BOOKING_CATEGORIES[notice.getBooking().getCategory()-1],bbid,lsu, user.getOpenid(), notice.getId().toString());
                      if(tmp!=null)WeixinUtil.sendTemplateNews2(tmp, act.getToken());  
                   }
                }
                if(hasText(user.getDeviceid())){
                    try {
                        BasicDevice device=new BasicDevice(user.getDeviceid());
                        app_messages.add(device);
                    } catch (InvalidDeviceTokenFormatException ex) {
                    }
                }
            }
            if(!messages.isEmpty())pushService.send_mqtt_topic(topics,messages);
            if(!app_messages.isEmpty()){
                try {
                    PushNotificationPayload py = new PushNotificationPayload();
                    py.addCustomDictionary("title", trim(notice.getTitle()));
                    py.addCustomDictionary("cat", notice.getSubtype());
                    py.addCustomDictionary("nid", notice.getId().toString());
                    py.addCustomDictionary("status", lsu);
                    py.addCustomDictionary("bid", notice.getBooking().getId().toString());
                    py.addAlert(trim(notice.getTitle()));
                    py.addSound("default");
                    py.addBadge(1);
                    Map m = new HashMap();
                    m.put("payload", py);
                    m.put("devices", app_messages);
                    pushService.send_apple_push_message(m);
                } catch (JSONException ex) {}
            }
        }
    }

    @Override
    public void notice_pay_success(String data) {
        if(!hasText(data))return;
        List<Device> app_messages=new ArrayList();
        String[] ids=StringUtils.split(data, "/");
        for (int i = 0; i < ids.length; i++) {
            String oo = ids[i];
            TUserNotice notice=userNRepo.findByMessage(oo);
            if(notice==null)continue;
            List<TUser> users=notice.getRecipients();
            List<String> messages=new ArrayList<>();
            List<String> topics=new ArrayList<>();
            for (TUser user : users) {
                if(hasText(user.getTopicid())){
                    Map m=new HashMap();
                    m.put("bid", notice.getBooking().getId());
                    m.put("title", notice.getTitle());
                    m.put("nid", notice.getId().toString());
                    m.put("cat", notice.getSubtype());
                    ObjectMapper om=new ObjectMapper();
                    try {
                        messages.add(messageHelper.securityChar()+om.writeValueAsString(m)+messageHelper.securityChar());
                        topics.add(user.getTopicid());
                    } catch (JsonProcessingException ex) {}
                }
                if(hasText(user.getOpenid())){
                   Float price;
                   AccessToken act=WeixinUtil.getAccessToken(WEIXIN_APP_ID, WEIXIN_APP_SEC);
                   if(act!=null){
                       if(notice.getBooking().getCategory()<=BOOKING_CATEGORY_LC)
                           price=notice.getBooking().getExpectedAmount();
                       else price=notice.getBooking().getOrderAmount();
                      Template tmp=messageHelper.createWeixinBookingCancelNotice(2,notice.getTitle(), com.wwzz.services.utils.Constants.BOOKING_CATEGORIES[notice.getBooking().getCategory()-1], Utils.priceToString(price)+"", 
                                            Utils.dateToString(notice.getBooking().getServiceTime()), user.getOpenid(), notice.getId().toString());
                      if(tmp!=null)WeixinUtil.sendTemplateNews(tmp, act.getToken());  
                   }
                }
                if(hasText(user.getDeviceid())){
                    try {
                        BasicDevice device=new BasicDevice(user.getDeviceid());
                        app_messages.add(device);
                    } catch (InvalidDeviceTokenFormatException ex) {
                    }
                }
            }
            pushService.send_mqtt_topic(topics,messages);
            try {
                PushNotificationPayload py = new PushNotificationPayload();
                py.addCustomDictionary("title", trim(notice.getTitle()));
                py.addCustomDictionary("bid", notice.getBooking().getId().toString());
                py.addCustomDictionary("cat", notice.getSubtype());
                py.addCustomDictionary("nid", notice.getId().toString());
                py.addAlert(trim(notice.getTitle()));
                py.addSound("default");
                py.addBadge(1);
                Map m = new HashMap();
                m.put("payload", py);
                m.put("devices", app_messages);
                pushService.send_apple_push_message(m);
            } catch (JSONException ex) {}
            
        }
    }

}
