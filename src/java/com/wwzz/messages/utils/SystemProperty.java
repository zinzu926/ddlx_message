/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.messages.utils;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author JBOSS
 */
public class SystemProperty implements Serializable{
    private String key;
    private String value;

    public SystemProperty(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj==null)return false;
        if(!(obj instanceof SystemProperty))return false;
        SystemProperty other=(SystemProperty) obj;
        return this.key.equalsIgnoreCase(other.getKey()); 
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.key);
        return hash;
    }
    
    
}
