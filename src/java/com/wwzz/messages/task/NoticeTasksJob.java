/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.messages.task;

import com.wwzz.services.api.ScheduleServices;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;


/**
 *
 * @author JBOSS
 */
public class NoticeTasksJob extends QuartzJobBean{
    
    private ScheduleServices noticeTask;
    
    @Override
    protected void executeInternal(JobExecutionContext jec) throws JobExecutionException {
        noticeTask.change_journey_state();
    }

    public void setNoticeTask(ScheduleServices noticeTask) {
        this.noticeTask = noticeTask;
    }
    
}
