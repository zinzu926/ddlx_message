/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.messages.task;

import com.wwzz.messages.MessageHelper;
import com.wwzz.messages.PushNotification;
import com.wwzz.services.domain.booking.TUserBookingOrder;
import com.wwzz.services.domain.booking.TUserOrderRestaurant;
import com.wwzz.services.domain.booking.TUserOrderShop;
import com.wwzz.services.domain.booking.TUserOrderTicket;
import com.wwzz.services.domain.news.TUserNotice;
import com.wwzz.services.domain.users.TUser;
import com.wwzz.services.repository.TUserBookingOrderRepository;
import com.wwzz.services.repository.TUserNoticeRepository;
import com.wwzz.services.repository.TUserOrderRestaurantRepository;
import com.wwzz.services.repository.TUserOrderShopRepository;
import com.wwzz.services.repository.TUserOrderTicketRepository;
import com.wwzz.services.repository.TUserRepository;
import com.wwzz.services.utils.Constants;
import com.wwzz.weixin.AccessToken;
import com.wwzz.weixin.Template;
import com.wwzz.weixin.utils.WeixinUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;


/**
 *
 * @author JBOSS
 */
@Transactional
public class BookingTasks implements Constants{
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private TUserRepository userRepo;
    @Autowired
    private TUserNoticeRepository noticeRepo;
    @Autowired
    @Qualifier("pushService")
    private PushNotification pushService;
    @Autowired
    @Qualifier("messageHelper")
    private MessageHelper messageHelper;
    @Autowired
    private TUserOrderRestaurantRepository orderResRepo;
    @Autowired
    private TUserOrderTicketRepository orderTktRepo;
    @Autowired
    private TUserOrderShopRepository orderShop;
    
    @Transactional
    public void schedule_task(){
        String sql="select p from TUserBookingOrder p where (p.status between 3 and 5) and p.journey.status=4 and "
                + "EXISTS (select r from TUserJourneyDaily r where r.journey.id=p.journey.id and r.journeyDate=:today) and "
                + "((FUNCTION('UNIX_TIMESTAMP',p.serviceTime)-:dt)/60 between 10 and 60) and "
                + "NOT EXISTS (select n from TUserNotice n where n.booking.id=p.id and n.type=2 and n.subtype>1)";
        List<TUserBookingOrder> booking=em.createQuery(sql)
                .setParameter("dt", System.currentTimeMillis()/1000L)
                .setParameter("today", new Date())
                .getResultList();
        StringBuilder sb=new StringBuilder();
        for (TUserBookingOrder bk : booking) {
            TUserNotice notice=new TUserNotice();
            notice.setBooking(bk);
            notice.setType(Constants.NOTICE_TYPE_2);
            notice.setMessage(UUID.randomUUID().toString());
            notice.setSender(userRepo.find_admin());
            notice.setSendTime(new Date());
            notice.setFlag(1);
            String name;
            String title;
            switch(bk.getCategory()){
                case BOOKING_CATEGORY_RESTURANT:
                    TUserOrderRestaurant rr=orderResRepo.findByBooking(bk);
                    name=rr.getRestaurant().getNameZh();
                    title=messageHelper.getResource("notice.jr.booking.2", name);
                    notice.setSubtype(Constants.JOURNEY_NOTICE_CATEGORY_3);
                    break;
                case BOOKING_CATEGORY_SS_PIAO:
                    TUserOrderTicket tt=orderTktRepo.findByBooking(bk);
                    name=tt.getScenic().getNameZh();
                    title=messageHelper.getResource("notice.jr.booking.3", name);
                    notice.setSubtype(Constants.JOURNEY_NOTICE_CATEGORY_4);
                    break;
                case BOOKING_CATEGORY_SHOP:
                    TUserOrderShop ss=orderShop.findByBooking(bk);
                    name=ss.getShop().getNameZh();
                    title=messageHelper.getResource("notice.jr.booking.4", name);
                    notice.setSubtype(Constants.JOURNEY_NOTICE_CATEGORY_5);
                    break;
                default:
                    name=BOOKING_CATEGORIES[bk.getCategory()-1];
                    title=messageHelper.getResource("notice.jr.booking.1", name);
                    notice.setSubtype(Constants.JOURNEY_NOTICE_CATEGORY_2);
                    break;
            }
            notice.setTitle(title);
            notice.setContent(notice.getTitle());
            notice.setCreated(new Date());
            notice.setModified(new Date());
            List<TUser> users=new ArrayList();
            users.add(bk.getApplicant());
            notice.setRecipients(users);
            noticeRepo.save(notice);
            sb.append(notice.getMessage()).append("/");
        }
        notice_cancel_no_bidder_booking(StringUtils.substringBeforeLast(sb.toString(), "/"));
    }
    
    public void notice_cancel_no_bidder_booking(String data) {
        if(!org.springframework.util.StringUtils.hasText(data))return;
        String[] ids=StringUtils.split(data, "/");
        for (int i = 0; i < ids.length; i++) {
            String oo = ids[i];
            TUserNotice nt=noticeRepo.findByMessage(oo);
            if(nt==null)continue;
            List<TUser> users=nt.getRecipients();
            TUserBookingOrder bo=nt.getBooking();
            String msgs=messageHelper.createMqttMessage(nt,bo.getId().toString(),bo.getCategory());
            List<String> mts=new ArrayList<>();
            for (TUser user : users) {
                if(hasText(user.getOpenid())){
                    AccessToken act=WeixinUtil.getAccessToken(WEIXIN_APP_ID, WEIXIN_APP_SEC);
                    if(act!=null){
                        Template tmp=messageHelper.createWeixinBookingTemplate(nt.getBooking(), user.getOpenid(), 2,nt.getId().toString());
                        if(tmp!=null)WeixinUtil.sendTemplateNews(tmp, act.getToken());
                    }
                }
                if(hasText(user.getTopicid()))mts.add(user.getTopicid());
            }
            if(!mts.isEmpty()){
                pushService.send_mqtt_topic(mts,msgs);
            }
            pushService.send_apple_push_message(messageHelper.createAppleMessage(users, nt));
        }
    }
    
    private String trim(String s){
        if(s==null)return null;
        if(s.length()>20)return s.substring(0, 15)+"...";
        else return s;
    }
    
    public boolean hasText(String s) {
        return org.springframework.util.StringUtils.hasText(s);
    }
}
