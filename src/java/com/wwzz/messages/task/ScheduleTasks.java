/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.messages.task;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wwzz.messages.MessageHelper;
import com.wwzz.messages.PushNotification;
import com.wwzz.services.api.ScheduleServices;
import com.wwzz.services.domain.booking.TUserBookingOrder;
import com.wwzz.services.domain.journey.TUserJourneyDaily;
import com.wwzz.services.domain.journey.TUserJourneyDailyCity;
import com.wwzz.services.domain.news.TUserNotice;
import com.wwzz.services.domain.users.TUser;
import com.wwzz.services.repository.TUserBookingOrderRepository;
import com.wwzz.services.repository.TUserNoticeRepository;
import com.wwzz.services.repository.TUserRepository;
import com.wwzz.services.utils.Constants;
import static com.wwzz.services.utils.Constants.BOOKING_CATEGORIES;
import static com.wwzz.services.utils.Constants.BOOKING_CATEGORY_LC;
import static com.wwzz.services.utils.Constants.WEIXIN_APP_ID;
import static com.wwzz.services.utils.Constants.WEIXIN_APP_SEC;
import com.wwzz.services.utils.Utils;
import com.wwzz.weixin.AccessToken;
import com.wwzz.weixin.Template;
import com.wwzz.weixin.utils.WeixinUtil;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import javapns.devices.Device;
import javapns.devices.exceptions.InvalidDeviceTokenFormatException;
import javapns.devices.implementations.basic.BasicDevice;
import javapns.notification.PushNotificationPayload;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author JBOSS
 */
@Service
@Transactional
public class ScheduleTasks implements ScheduleServices{
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private MessageSource resources;
    @Autowired
    private TUserRepository userRepo;
    @Autowired
    private TUserNoticeRepository noticeRepo;
    @Autowired
    @Qualifier("pushService")
    private PushNotification pushService;
    @Autowired
    @Qualifier("messageHelper")
    private MessageHelper messageHelper;
    @Autowired
    private TUserBookingOrderRepository bookRepo;
    
    private static final Logger logger=LoggerFactory.getLogger(ScheduleTasks.class);
    
    @Override
    @Transactional
    public void change_journey_state() {
        Date dd=new Date();
        logger.info("Started ScheduleTask....",dd.toString());
        String sql="update TUserJourney p set p.status=4 where (p.status=2 or p.status=3) and current_date between p.beginTime and p.endTime";
        int n=em.createQuery(sql).executeUpdate();
        logger.info("Updated journey state: ",n);
        sql="update TUserJourney p set p.status=5 where p.status=4 and current_date>p.endTime";
        n=em.createQuery(sql).executeUpdate();
        logger.info("Updated journey state: ",n);
        Calendar cal=Calendar.getInstance();
        cal.add(Calendar.DATE, 1);
        Date dt=cal.getTime();
        sql="select p from TUserJourneyDaily p where p.journeyDate=:dt and p.journey.status=4";
        List lst=em.createQuery(sql).setParameter("dt", dt,TemporalType.DATE).getResultList();
        List<String> messages = new ArrayList<>();
        List<String> topics = new ArrayList<>();
        List app_messages = new ArrayList();
        for (int i = 0; i < lst.size(); i++) {
            TUserJourneyDaily jd = (TUserJourneyDaily) lst.get(i);
            TUserNotice notice=new TUserNotice();
            List<TUserJourneyDailyCity> cities=jd.getCities();
            StringBuilder sb=new StringBuilder();
            for (TUserJourneyDailyCity city : cities) {
                sb.append(city.getCity().getName1()).append(",");
            }
            String ss=StringUtils.substringBeforeLast(sb.toString(), ",");
            notice.setTitle(resources.getMessage("notice.jr.city", new String[]{ss}, new Locale("zh", "CN")));
            notice.setContent(notice.getTitle());
            notice.setSubtype(Constants.JOURNEY_NOTICE_CATEGORY_1);
            notice.setType(Constants.NOTICE_TYPE_2);
            notice.setCreated(new Date());
            notice.setJourney(jd);
            notice.setSendTime(new Date());
            notice.setSender(userRepo.find_admin());
            notice.setMessage(UUID.randomUUID().toString());
            notice.setModified(new Date());
            List<TUser> users=new ArrayList();
            users.add(jd.getJourney().getUser());
            notice.setRecipients(users);
            noticeRepo.save(notice);
            TUser user=jd.getJourney().getUser();
            if (org.springframework.util.StringUtils.hasText(user.getTopicid())) {
                Map m = new HashMap();
                m.put("title", notice.getTitle());
                m.put("cat", 500);
                m.put("nid", notice.getId().toString());
                m.put("bbid", jd.getJourney().getId().toString());
                ObjectMapper om = new ObjectMapper();
                try {
                    messages.add(messageHelper.securityChar() + om.writeValueAsString(m) + messageHelper.securityChar());
                    topics.add(user.getTopicid());
                } catch (JsonProcessingException ex) {}
            }
            List<Device> devices=new ArrayList();
            if (org.springframework.util.StringUtils.hasText(user.getDeviceid())) {
                try {
                    BasicDevice device = new BasicDevice(user.getDeviceid());
                    devices.add(device);
                } catch (InvalidDeviceTokenFormatException ex) {}
            }
            PushNotificationPayload py = new PushNotificationPayload();
            try {
                py.addCustomDictionary("title", notice.getTitle());
                py.addCustomDictionary("cat", 500);
                py.addCustomDictionary("bbid", jd.getJourney().getId().toString());
                py.addCustomDictionary("nid", notice.getId().toString());
                py.addSound("default");
                py.addBadge(1);
                Map m = new HashMap();
                m.put("payload", py);
                m.put("devices", devices);
                app_messages.add(m);
            } catch (JSONException ex) {}
        }
        
        pushService.send_apple_push_message_batch(app_messages);
        pushService.send_mqtt_topic(topics,messages);
        logger.info("Finished ScheduleTask....",new Date());
        //
        schedule_task();
    }
    
    public void notice_cancel_no_bidder_booking(String data) {
        if(!org.springframework.util.StringUtils.hasText(data))return;
        List<String> messages=new ArrayList<>();
        List<String> topics=new ArrayList<>();
        List<Device> app_messages=new ArrayList();
        String[] ids=StringUtils.split(data, "/");
        for (int i = 0; i < ids.length; i++) {
            String oo = ids[i];
            TUserNotice notice=noticeRepo.findByMessage(oo);
            if(notice==null)continue;
            List<TUser> users=notice.getRecipients();
            for (TUser user : users) {
                if(org.springframework.util.StringUtils.hasText(user.getTopicid())){
                    Map m=new HashMap();
                    m.put("title", notice.getTitle());
                    m.put("nid", notice.getId().toString());
                    m.put("cat", notice.getSubtype());
                    ObjectMapper om=new ObjectMapper();
                    try {
                        messages.add(messageHelper.securityChar()+om.writeValueAsString(m)+messageHelper.securityChar());
                        topics.add(user.getTopicid());
                    } catch (JsonProcessingException ex) {}
                }
                if(org.springframework.util.StringUtils.hasText(user.getOpenid())){
                   AccessToken act=WeixinUtil.getAccessToken(WEIXIN_APP_ID, WEIXIN_APP_SEC);
                   Float price;
                   if(act!=null){
                      if(notice.getBooking().getCategory()<=BOOKING_CATEGORY_LC)
                           price=notice.getBooking().getExpectedAmount();
                       else price=notice.getBooking().getOrderAmount();
                      Template tmp=messageHelper.createWeixinBookingCancelNotice(notice.getSubtype(),notice.getTitle(), notice.getBooking().getSerialNumber(), Utils.priceToString(price)+"", "" ,user.getOpenid(), notice.getId().toString());
                      if(tmp!=null)WeixinUtil.sendTemplateNews(tmp, act.getToken());  
                   }
                }
                if(org.springframework.util.StringUtils.hasText(user.getDeviceid())){
                    try {
                        BasicDevice device=new BasicDevice(user.getDeviceid());
                        app_messages.add(device);
                    } catch (InvalidDeviceTokenFormatException ex) {
                    }
                }
            }
            
            try {
                PushNotificationPayload py = new PushNotificationPayload();
                py.addCustomDictionary("title", trim(notice.getTitle()));
                py.addCustomDictionary("cat", notice.getSubtype());
                py.addCustomDictionary("nid", notice.getId().toString());
                py.addAlert(trim(notice.getTitle()));
                py.addSound("default");
                py.addBadge(1);
                Map m = new HashMap();
                m.put("payload", py);
                m.put("devices", app_messages);
                pushService.send_apple_push_message(m);
            } catch (JSONException ex) {}
            pushService.send_mqtt_topic(topics,messages);
        }
    }
    
    private String trim(String s){
        if(s==null)return null;
        if(s.length()>20)return s.substring(0, 15)+"...";
        else return s;
    }
    
    public void schedule_task(){
        String sql="select p from TUserBookingOrder p where p.category<=6 and p.status=1 and "
                + "(FUNCTION('UNIX_TIMESTAMP',p.applicationTime)-:dt)/3600>24 and "
                + "NOT EXISTS (select 1 from TUserBookingTempOrder n where n.booking.id=p.id)";
        List<TUserBookingOrder> booking=em.createQuery(sql).setParameter("dt", System.currentTimeMillis()/1000L).getResultList();
        StringBuilder sb=new StringBuilder();
        for (TUserBookingOrder bk : booking) {
            bk.setStatus(Constants.BOOKING_STATE_8);
            bookRepo.save(bk);
            TUserNotice notice=new TUserNotice();
            notice.setBooking(bk);
            notice.setType(Constants.NOTICE_TYPE_3);
            notice.setSubtype(Constants.SYSTEM_NOTICE_CATEGORY_2);
            notice.setMessage(UUID.randomUUID().toString());
            notice.setSender(userRepo.find_admin());
            notice.setSendTime(new Date());
            notice.setTitle(BOOKING_CATEGORIES[bk.getCategory()-1]+" 预约已过期。");
            notice.setContent(notice.getTitle());
            notice.setCreated(new Date());
            notice.setModified(new Date());
            List<TUser> users=new ArrayList();
            users.add(bk.getApplicant());
            notice.setRecipients(users);
            noticeRepo.save(notice);
            sb.append(notice.getMessage()).append("/");
        }
        notice_cancel_no_bidder_booking(StringUtils.substringBeforeLast(sb.toString(), "/"));
    }
}
