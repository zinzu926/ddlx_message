/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.messages.task;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;


/**
 *
 * @author JBOSS
 */
public class BookingTasksJob extends QuartzJobBean{
    
    private BookingTasks noticeTask;
    
    @Override
    protected void executeInternal(JobExecutionContext jec) throws JobExecutionException {
        noticeTask.schedule_task();
    }

    public void setNoticeTask(BookingTasks noticeTask) {
        this.noticeTask = noticeTask;
    }
    
}
