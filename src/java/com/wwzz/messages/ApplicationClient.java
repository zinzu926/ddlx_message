/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.messages;

import com.wwzz.messages.utils.SystemProperty;
import com.wwzz.services.domain.admin.TSystemProperty;
import com.wwzz.services.domain.apps.TAppsRegister;
import com.wwzz.services.utils.Blowfish;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author JBoss
 */
@Service
@Transactional
public class ApplicationClient {
    @PersistenceContext
    private  EntityManager em;
    
    public void load(){
        List data=new ArrayList();
        List<TAppsRegister> lst=em.createQuery("select p from TAppsRegister p").getResultList();
        lst.stream().map((lst1) -> (TAppsRegister) lst1).map((ap) -> {
            Map m=new HashMap();
            m.put("appKey", ap.getCertKeys());
            m.put("status", ap.getStatus());
            m.put("appId", ap.getAppsId());
            m.put("server", ap.getServer());
            m.put("type", ap.getType());
            return m;
        }).forEach((m) -> {
            data.add(m);
        });
        this.appInfos=data;
        load_system();
    }
    
    private List appInfos;

    public List getAppInfos() {
        return appInfos;
    }
    private List<SystemProperty> systemInfo;
    
    public String getAppKey(String app){
        if(this.appInfos==null)return null;
        for (int i = 0; i < appInfos.size(); i++) {
            Map mp = (Map) appInfos.get(i);
            String appid=(String) mp.get("appId");
            if(app.equals(appid)){
                return (String) mp.get("appKey");
            }
        }
        return null;
    }
    
    public String set_uid(String client,String id){
        Blowfish bw=new Blowfish();
        bw.setKey(getAppKey(client));
        return bw.encryptString(id);
    }
    
    public String get_uid(String client,String id){
        Blowfish bw=new Blowfish();
        bw.setKey(getAppKey(client));
        return bw.decryptString(id);
    }
      
    public boolean isWeixin(String app){
        if(this.appInfos!=null){
            for (Object appInfo : appInfos) {
                Map mp = (Map) appInfo;
                String appid=(String) mp.get("appId");
                String type=(String) mp.get("type");
                if(app.equals(appid) && type.equals("weixin")){
                    return true;
                }
            }
        }
        return false;
    }
    
    public String getType(String app){
        if(this.appInfos!=null){
            for (Object appInfo : appInfos) {
                Map mp = (Map) appInfo;
                String appid=(String) mp.get("appId");
                String type=(String) mp.get("type");
                if(app.equals(appid)){
                    return type;
                }
            }
        }
        return null;
    }
      
    public void load_system(){
        List lst=em.createQuery("select p from TSystemProperty p").getResultList();
        systemInfo=new ArrayList();
        for (int i = 0; i < lst.size(); i++) {
            TSystemProperty sys = (TSystemProperty) lst.get(i);
            SystemProperty sp=new SystemProperty(sys.getKey(), sys.getValue());
            systemInfo.add(sp);
        }
    }
    
    public String getSystemProperty(String key){
        if(systemInfo==null)return null;
        for (SystemProperty sp : systemInfo) {
            if(sp.getKey().equals(key))return sp.getValue();
        }
        return null;
    }
    
    public void addSystemProperty(String key,String value){
        if(systemInfo==null)systemInfo=new ArrayList();
        SystemProperty sp=new SystemProperty(key, value);
        if(!systemInfo.contains(sp))systemInfo.add(sp);
    }
    
    public boolean isLoaded(){
        if(this.appInfos!=null && this.systemInfo!=null){
            return (!this.appInfos.isEmpty()) && (!this.systemInfo.isEmpty());
        }else return false;
    }
}
