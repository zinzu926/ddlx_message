/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.messages;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wwzz.services.domain.booking.TUserBookingOrder;
import com.wwzz.services.domain.booking.TUserBookingTempOrder;
import com.wwzz.services.domain.news.TUserNotice;
import com.wwzz.services.domain.users.TUser;
import com.wwzz.services.utils.Constants;
import static com.wwzz.services.utils.Constants.BOOKING_CATEGORIES;
import com.wwzz.services.utils.Utils;
import com.wwzz.weixin.Template;
import com.wwzz.weixin.Template2;
import com.wwzz.weixin.TemplateData;
import com.wwzz.weixin.TemplateData2;
import com.wwzz.weixin.TemplateItem;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import javapns.devices.Device;
import javapns.devices.exceptions.InvalidDeviceTokenFormatException;
import javapns.devices.implementations.basic.BasicDevice;
import javapns.notification.PushNotificationPayload;
import org.apache.velocity.app.VelocityEngine;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.ui.velocity.VelocityEngineUtils;
import org.springframework.util.StringUtils;

/**
 *
 * @author JBOSS
 */
public class MessageHelper implements Constants{
    @Autowired
    private MessageSource resources;
    
    private ApplicationClient systems;

    public void setSystems(ApplicationClient systems) {
        this.systems = systems;
        if(this.systems!=null){
            if(!this.systems.isLoaded())this.systems.load();
        }
    }
    
    public String securityChar(){
        char ch=0x266B;
        StringBuilder sb=new StringBuilder();
        sb.append(ch).append(ch).append(ch).append(ch).append(ch);
        return sb.toString();
    }
    
    public Template createWeixinBookingTemplate(Object booking,String openid,int state,String nid){
        Template temp=new Template();
        temp.setTopcolor("#FF0000");
        StringBuilder sb=new StringBuilder();
        sb.append(systems.getSystemProperty("template.url.prefix")).append("weixin/notify?state=").append(state).append("&openid=");
        temp.setTouser(openid);
        TemplateData dd=new TemplateData();
        TemplateItem item;
        //예약자에게 통지보내기
        String cat;
        if(state==1){
            TUserBookingTempOrder tb=(TUserBookingTempOrder) booking;
            //
            item=new TemplateItem();
            item.setValue(getResource("notice.bidder.title",BOOKING_CATEGORIES[tb.getBooking().getCategory()-1]));
            item.setColor("#173177");
            dd.setFirst(item);
            //
            item=new TemplateItem();
            item.setValue(getResource("notice.bidder.cat",BOOKING_CATEGORIES[tb.getBooking().getCategory()-1]));
            item.setColor("#235468");
            dd.setKeyword1(item);
            //
            item=new TemplateItem();
            item.setValue(Utils.dateToString(tb.getSubmissionTime()));
            item.setColor("#235468");
            dd.setKeyword2(item);
            //
            item=new TemplateItem();
            item.setValue(getResource("notice.bidder.remark"));
            item.setColor("#235468");
            dd.setRemark(item);
            //
            sb.append(openid).append("&xfdf=").append(tb.getId()).append("&cjh=").append(nid);
            temp.setUrl(sb.toString());
            temp.setData(dd);
            temp.setTemplate_id(systems.getSystemProperty("weixin.template.2"));
            return temp;
        }else
        //
        if(state==2){
            item=new TemplateItem();
            item.setValue(getResource("template.title"));
            item.setColor("#173177");
            dd.setFirst(item);
            TUserBookingOrder bk=(TUserBookingOrder) booking;
            //
            item=new TemplateItem();
            item.setColor("#235468");
            item.setValue(bk.getSerialNumber());
            dd.setKeyword1(item);
            //
            item=new TemplateItem();
            item.setColor("#235468");
            item.setValue(Utils.priceToString(bk.getExpectedAmount()));
            dd.setKeyword2(item);
            //
            item=new TemplateItem();
            Map m=new HashMap();
            cat=BOOKING_CATEGORIES[bk.getCategory()-1];
            m.put("cat",cat);
            String v=getDetailInfo("temp1", m);
            item.setValue(v);
            item.setColor("#235468");
            dd.setRemark(item);
            //
            sb.append(openid).append("&xfdf=").append(bk.getId()).append("&cjh=").append(nid);
            temp.setUrl(sb.toString());
            temp.setData(dd);
            temp.setTemplate_id(systems.getSystemProperty("weixin.template.4"));
            return temp;
        }else return null;
    }
     //
    public String createMqttMessage(TUserNotice notice,String bbid,int cat){
        Map m=new HashMap();
        m.put("msgid", UUID.randomUUID().toString());
        m.put("title", notice.getTitle());
        m.put("nid", notice.getId().toString());
        m.put("bcat", cat);
        m.put("bbid", bbid);
        m.put("cat", 1);
        ObjectMapper om=new ObjectMapper();
        try {
            return securityChar()+om.writeValueAsString(m)+securityChar();
        } catch (JsonProcessingException ex) {
            return null;
        }
    }
    
    public String getResource(String key) {
        try{
            return resources.getMessage(key, null, new Locale("zh", "CN"));
        }catch(NoSuchMessageException ex){
            return null;
        }
    }

    public String getResource(String key, String... values) {
        try{
            return resources.getMessage(key, values, new Locale("zh", "CN"));
        }catch(NoSuchMessageException ex){
            return null;
        }
    }
    
    private String getDetailInfo(String vm, Map data){
        String ss="resources/"+vm+".vm";
        String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, ss,"UTF-8", data);
        return text;
    }
    
    @Autowired
    private VelocityEngine velocityEngine;
    
    public Map createAppleMessage(List<TUser> users,TUserNotice notice){
        System.out.println("Users:"+users);
        List<Device> messages=new ArrayList();
        PushNotificationPayload py = null;
        if(!users.isEmpty() && notice!=null){
        TUserBookingOrder booking=notice.getBooking();
        py=new PushNotificationPayload();
        try {
            py.addCustomDictionary("title", trim(notice.getTitle()));
            py.addCustomDictionary("nid", notice.getId().toString());
            py.addCustomDictionary("bcat", booking.getCategory());
            py.addCustomDictionary("bbid", booking.getId().toString());
            py.addCustomDictionary("cat", 1);
            py.addAlert(trim(notice.getTitle()));
            py.addSound("default");
            py.addBadge(1);
        } catch (JSONException ex) {
            
        }
        for (TUser user : users) {
            if(StringUtils.hasText(user.getDeviceid())){
                try {
                    BasicDevice device=new BasicDevice(user.getDeviceid());
                    messages.add(device);
                } catch (InvalidDeviceTokenFormatException ex) {
                }
            }
        }
        }
        Map m=new HashMap();
        m.put("payload", py);
        m.put("devices", messages);
        return m;
    }
    
    public Map createAppleBidMessage(List<TUser> users,TUserBookingTempOrder temp){
        List<Device> messages=new ArrayList();
        PushNotificationPayload py = null;
        if(!users.isEmpty() && temp!=null){
        TUserBookingOrder booking=temp.getBooking();
        py=new PushNotificationPayload();
        try {
            py.addCustomDictionary("title", trim(booking.getTitle()));
            py.addCustomDictionary("bcat", booking.getCategory());
            py.addCustomDictionary("bbid", temp.getId().toString());
            py.addCustomDictionary("bid", temp.getBooking().getId().toString());
            py.addCustomDictionary("cat", 200);
            py.addAlert(trim(booking.getTitle()));
            py.addSound("default");
            py.addBadge(1);
        } catch (JSONException ex) {
            
        }
        for (TUser user : users) {
            if(StringUtils.hasText(user.getDeviceid())){
                try {
                    BasicDevice device=new BasicDevice(user.getDeviceid());
                    messages.add(device);
                } catch (InvalidDeviceTokenFormatException ex) {
                }
            }
        }
        }
        Map m=new HashMap();
        m.put("payload", py);
        m.put("devices", messages);
        return m;
    }
      
    public String mqtt_message_by_booking_cancel(TUserNotice notice,String bbid){
        Map m=new HashMap();
        m.put("msgid", UUID.randomUUID().toString());
        m.put("title", notice.getTitle());
        m.put("bbid", bbid);
        m.put("nid", notice.getId().toString());
        m.put("cat", 100);
        ObjectMapper om=new ObjectMapper();
        try {
            return securityChar()+om.writeValueAsString(m)+securityChar();
        } catch (JsonProcessingException ex) {
            return null;
        }
    }
    
    public Map apple_message_by_book_cancel(List<TUser> users,TUserNotice notice,String bid){
        List<Device> messages=new ArrayList();
        PushNotificationPayload py = null;
        if(!users.isEmpty() && notice!=null){
        py=new PushNotificationPayload();
        try {
            py.addCustomDictionary("title", trim(notice.getTitle()));
            py.addCustomDictionary("bbid", bid);
            py.addCustomDictionary("nid", notice.getId().toString());
            py.addCustomDictionary("cat", 100);
            py.addAlert(trim(notice.getTitle()));
            py.addSound("default");
        } catch (JSONException ex) {
            
        }
        for (TUser user : users) {
            if(StringUtils.hasText(user.getDeviceid())){
                try {
                    BasicDevice device=new BasicDevice(user.getDeviceid());
                    messages.add(device);
                } catch (InvalidDeviceTokenFormatException ex) {
                }
            }
        }
        }
        Map m=new HashMap();
        m.put("payload", py);
        m.put("devices", messages);
        return m;
    }
     
    public String createBidMqttMessage(TUserBookingTempOrder booking){
        Map m=new HashMap();
        m.put("msgid", UUID.randomUUID().toString());
        m.put("title", booking.getBooking().getTitle());
        m.put("bcat", booking.getBooking().getCategory());
        m.put("bbid", booking.getId().toString());
        m.put("bid", booking.getBooking().getId().toString());
        m.put("cat", 200);
        ObjectMapper om=new ObjectMapper();
        try {
            return securityChar()+om.writeValueAsString(m)+securityChar();
        } catch (JsonProcessingException ex) {
            return null;
        }
    }
    
    private String trim(String s){
        if(s==null)return null;
        if(s.length()>20)return s.substring(0, 15)+"...";
        else return s;
    }
    
    public Template createWeixinJourneyNotice(String title,int cat,String jid,String openid,String nid){
        String tempId=systems.getSystemProperty("weixin.template.1");
        System.out.println("TempID:"+tempId);
        Template temp=new Template();
        temp.setTopcolor("#FF0000");
        StringBuilder sb=new StringBuilder();
        sb.append(systems.getSystemProperty("template.url.prefix")).append("weixin/notify?state=").append(5).append("&openid=");
        sb.append(openid).append("&jjh=").append(jid).append("&qwer=").append(cat).append("&cjh=").append(nid);
        temp.setTouser(openid);
        TemplateData dd=new TemplateData();
        TemplateItem item;
        if(cat==Constants.SYSTEM_NOTICE_CATEGORY_13){
            item=new TemplateItem();
            item.setValue(resources.getMessage("notice.jr.title.1", null, new Locale("zh","CN")));
            item.setColor("#094701");
            dd.setFirst(item);
            //
            item=new TemplateItem();
            item.setValue(title);
            item.setColor("#173177");
            dd.setKeyword1(item);
            //
            item=new TemplateItem();
            item.setValue(Utils.dateToString(new Date()));
            item.setColor("#173177");
            dd.setKeyword2(item);
            //
            item=new TemplateItem();
            item.setValue(resources.getMessage("notice.jr.remark.1", null, new Locale("zh","CN")));
            item.setColor("#173177");
            dd.setRemark(item);
        }else if(cat==Constants.SYSTEM_NOTICE_CATEGORY_12){
            item=new TemplateItem();
            item.setValue(resources.getMessage("notice.jr.title.2", null, new Locale("zh","CN")));
            item.setColor("#094701");
            dd.setFirst(item);
            //
            item=new TemplateItem();
            item.setValue(title);
            item.setColor("#173177");
            dd.setKeyword1(item);
            //
            item=new TemplateItem();
            item.setValue(Utils.dateToString(new Date()));
            item.setColor("#173177");
            dd.setKeyword2(item);
            //
            item=new TemplateItem();
            item.setValue(resources.getMessage("notice.jr.remark.2", null, new Locale("zh","CN")));
            item.setColor("#173177");
            dd.setRemark(item);
        }else return null;
        temp.setUrl(sb.toString());
        temp.setData(dd);
        temp.setTemplate_id(tempId);
        return temp;
    }
    
    public Template createWeixinSkillNotice(int cat,String uname,String ref, String phone,String openid,String nid){
        String tempId=systems.getSystemProperty("weixin.template.5");
        Template temp=new Template();
        temp.setTopcolor("#FF0000");
        StringBuilder sb=new StringBuilder();
        sb.append(systems.getSystemProperty("template.url.prefix")).append("weixin/notify?state=").append(3).append("&openid=");
        sb.append(openid).append("&qwer=").append(cat).append("&cjh=").append(nid);
        temp.setTouser(openid);
        TemplateData dd=new TemplateData();
        TemplateItem item;
        //Skill pass
        if(cat==Constants.SYSTEM_NOTICE_CATEGORY_10){
            item=new TemplateItem();
            item.setValue(resources.getMessage("notice.skill.ok.title", null, new Locale("zh","CN")));
            item.setColor("#820101");
            dd.setFirst(item);
            //
            item=new TemplateItem();
            item.setValue(uname);
            item.setColor("#173177");
            dd.setKeyword1(item);
            //
            item=new TemplateItem();
            item.setValue(ref);
            item.setColor("#173177");
            dd.setKeyword2(item);
            //
            item=new TemplateItem();
            item.setValue(phone);
            item.setColor("#173177");
            dd.setKeyword3(item);
            //
            item=new TemplateItem();
            item.setValue(Utils.dateToString(new Date()));
            item.setColor("#173177");
            dd.setKeyword4(item);
            //
            item=new TemplateItem();
            item.setValue(resources.getMessage("notice.skill.ok.remark", null, new Locale("zh","CN")));
            item.setColor("#173177");
            dd.setRemark(item);
        }else if(cat==Constants.SYSTEM_NOTICE_CATEGORY_11){
            item=new TemplateItem();
            item.setValue(resources.getMessage("notice.skill.fail.title", null, new Locale("zh","CN")));
            item.setColor("#820101");
            dd.setFirst(item);
            //
            item=new TemplateItem();
            item.setValue(uname);
            item.setColor("#173177");
            dd.setKeyword1(item);
            //
            item=new TemplateItem();
            item.setValue(ref);
            item.setColor("#173177");
            dd.setKeyword2(item);
            //
            item=new TemplateItem();
            item.setValue(phone);
            item.setColor("#173177");
            dd.setKeyword3(item);
            //
            item=new TemplateItem();
            item.setValue(Utils.dateToString(new Date()));
            item.setColor("#173177");
            dd.setKeyword4(item);
            //
            item=new TemplateItem();
            item.setValue(resources.getMessage("notice.skill.fail.remark", null, new Locale("zh","CN")));
            item.setColor("#173177");
            dd.setRemark(item);
        }else return null;
        temp.setUrl(sb.toString());
        temp.setData(dd);
        temp.setTemplate_id(tempId);
        return temp;
    }
    
    public Template createWeixinBookingCancelNotice(int cat,String title,String order_no, String money, String time, String openid,String nid){
        String tempId;
        Template temp=new Template();
        temp.setTopcolor("#FF0000");
        StringBuilder sb=new StringBuilder();
        sb.append(systems.getSystemProperty("template.url.prefix")).append("weixin/notify?state=").append(4).append("&openid=");
        sb.append(openid).append("&qwer=").append(cat).append("&cjh=").append(nid);
        temp.setTouser(openid);
        TemplateData dd=new TemplateData();
        TemplateItem item;
        //by user
        if(cat==100){
            tempId=systems.getSystemProperty("weixin.template.10");
            
            item=new TemplateItem();
            item.setValue(title);
            item.setColor("#d60000");
            dd.setFirst(item);
            //
            item=new TemplateItem();
            item.setValue(order_no);
            item.setColor("#173177");
            dd.setKeyword1(item);
            //
            item=new TemplateItem();
            item.setValue(money);
            item.setColor("#173177");
            dd.setKeyword2(item);
            //
            item=new TemplateItem();
            item.setValue("");
            item.setColor("#173177");
            dd.setRemark(item);
        }else if(cat==2){
            tempId=systems.getSystemProperty("weixin.template.7");
            
            item=new TemplateItem();
            item.setValue(title);
            item.setColor("#d60000");
            dd.setFirst(item);
            //
            item=new TemplateItem();
            item.setValue(order_no);
            item.setColor("#173177");
            dd.setKeyword1(item);
            //
            item=new TemplateItem();
            item.setValue(money);
            item.setColor("#173177");
            dd.setKeyword2(item);
            //
            item=new TemplateItem();
            item.setValue(time);
            item.setColor("#173177");
            dd.setKeyword3(item);
            //
            item=new TemplateItem();
            item.setValue("");
            item.setColor("#173177");
            dd.setRemark(item);
        }else return null;
        temp.setUrl(sb.toString());
        temp.setData(dd);
        temp.setTemplate_id(tempId);
        return temp;
    }
    //
    public Template2 createWeixinPayReadyNotice(int cat,String company,String order_type,String bbid,String lsu,String openid,String nid){
        String tempId;
        Template2 temp=new Template2();
        temp.setTopcolor("#FF0000");
        StringBuilder sb=new StringBuilder();
        sb.append(systems.getSystemProperty("template.url.prefix")).append("weixin/notify?state=").append(7).append("&openid=");
        sb.append(openid).append("&qwer=").append(cat).append("&cjh=").append(nid);
        sb.append("&xfdf=").append(bbid).append("&lsu=").append(lsu);
        temp.setTouser(openid);
        TemplateData2 dd=new TemplateData2();
        TemplateItem item;
 
        tempId=systems.getSystemProperty("weixin.template.6");

        item=new TemplateItem();
        item.setValue(getResource("notice.pay.ready.title"));
        item.setColor("#d60000");
        dd.setFirst(item);
        //
        item=new TemplateItem();
        item.setValue(getResource("notice.pay.ready.type"));
        item.setColor("#173177");
        dd.setType(item);
        //
        item=new TemplateItem();
        item.setValue(company);
        item.setColor("#173177");
        dd.setName(item);
        //
        item=new TemplateItem();
        item.setValue(getResource("notice.pay.ready.product.type"));
        item.setColor("#173177");
        dd.setProductType(item);
        //
        item=new TemplateItem();
        item.setValue(order_type);
        item.setColor("#173177");
        dd.setServiceName(item);
        //
        item=new TemplateItem();
        item.setValue(Utils.dateToString(new Date()));
        item.setColor("#173177");
        dd.setTime(item);
        //
        item=new TemplateItem();
        item.setValue(getResource("notice.pay.ready.remark"));
        item.setColor("#173177");
        dd.setRemark(item);
    
        temp.setUrl(sb.toString());
        temp.setData(dd);
        temp.setTemplate_id(tempId);
        return temp;
    }
}
