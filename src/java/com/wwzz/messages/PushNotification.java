/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.messages;

import com.wwzz.services.utils.Constants;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import javapns.communication.exceptions.CommunicationException;
import javapns.communication.exceptions.KeystoreException;
import javapns.devices.Device;
import javapns.notification.AppleNotificationServer;
import javapns.notification.AppleNotificationServerBasicImpl;
import javapns.notification.PushNotificationManager;
import javapns.notification.PushNotificationPayload;
import org.fusesource.hawtbuf.Buffer;
import org.fusesource.hawtbuf.UTF8Buffer;
import org.fusesource.mqtt.client.FutureConnection;
import org.fusesource.mqtt.client.MQTT;
import org.fusesource.mqtt.client.QoS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author JBOSS
 */
public class PushNotification implements Constants{
    
    private ApplicationClient systems;

    public void setSystems(ApplicationClient systems) {
        this.systems = systems;
        if(this.systems!=null)this.systems.load();
    }
    
    private static final Logger logger = LoggerFactory.getLogger(PushNotification.class);
    
    private boolean appleconnected=false;
    private MQTT mqtt;
    private FutureConnection mqttCon;

    public PushNotification(ApplicationClient systems) {
        this.systems = systems;
        if(this.systems!=null){
            if(!this.systems.isLoaded())this.systems.load();
        }
        this.pushManager = new PushNotificationManager();
        mqtt_connect();
    }
      
    private void init_push_server(){
        try {
            pushServer=new AppleNotificationServerBasicImpl(systems.getSystemProperty("apple.push.cert"), 
                        systems.getSystemProperty("apple.push.key"), false);
            try {
                pushManager.initializeConnection(pushServer);
                appleconnected=true;
            } catch (CommunicationException ex) {
                logger.error("Fail apple push server..:"+ex.getMessage());
                appleconnected=false;
            }
        } catch (KeystoreException ex) {
            logger.error("Push key failed..:"+ex.getMessage());
            appleconnected=false;
        }
        
        logger.info("Connected Apple push server..:");
    }
    public synchronized void send_apple_push_message(Map info){
        init_push_server();
        if(!appleconnected)return;
        PushNotificationPayload py=(PushNotificationPayload) info.get("payload");
        if(py!=null){
            List<Device>device=(List<Device>) info.get("devices");
            if(device==null)return;
            if(!device.isEmpty())try {
                pushManager.sendNotifications(py, device);
                pushManager.stopConnection();
                appleconnected=false;
            } catch (CommunicationException | KeystoreException ex) {
                logger.error("Failed send push message");
            }
        }
    }
    private PushNotificationManager pushManager=null;
    private AppleNotificationServer pushServer=null;
    
    public synchronized void send_apple_push_message_batch(List messages){
        if(messages.isEmpty())return;
        init_push_server();
        for (int i = 0; i < messages.size(); i++) {
             Map info = (Map) messages.get(i);
            PushNotificationPayload py=(PushNotificationPayload) info.get("payload");
            if(py!=null){
                List<Device>device=(List<Device>) info.get("devices");
                if(device==null)return;
                if(!device.isEmpty())try {
                    pushManager.sendNotifications(py, device);
                } catch (CommunicationException | KeystoreException ex) {
                    logger.error("Failed send push message");
                }
            }
        }
        try {
            pushManager.stopConnection();
        } catch (CommunicationException | KeystoreException ex) {
            logger.error("Failed send push message");
        }
        appleconnected=false;
    }
    
    public void send_mqtt_topic(List<String> topics,String message){
        if(!mqttCon.isConnected())mqtt_connect();
        Buffer msg = new UTF8Buffer(message);
        for (String topic1 : topics) {
            UTF8Buffer topic = new UTF8Buffer(topic1);
            mqttCon.publish(topic, msg, QoS.AT_MOST_ONCE, false);
        }
    }
    
    public void send_mqtt_topic(List<String> topics,List<String> message){
        if(!mqttCon.isConnected())mqtt_connect();
        for (int i=0;i<topics.size();i++) {
            String tp=topics.get(i);
            String msg1=message.get(i);
            Buffer msg = new UTF8Buffer(msg1);
            UTF8Buffer topic = new UTF8Buffer(tp);
            mqttCon.publish(topic, msg, QoS.AT_MOST_ONCE, false);
        }
    }
    private void init_mqtt(){
        mqtt = new MQTT();
        try {
            mqtt.setHost(systems.getSystemProperty("notification.server"), 1883);
            mqtt.setUserName(systems.getSystemProperty("notification.server.user"));
            mqtt.setPassword(systems.getSystemProperty("notification.server.key"));
            
        } catch (URISyntaxException ex) {
            
        }
    }
    private void mqtt_connect(){
        if(mqtt==null){
            init_mqtt();
        }
        mqttCon = mqtt.futureConnection();
        try {
            mqttCon.connect().await();
            logger.info("MQTT Connection OK...");
        } catch (Exception ex) {
            logger.info("MQTT Error:",ex.getMessage());
        }
    }
}
