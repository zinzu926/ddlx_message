/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.messages;

import com.wwzz.services.api.MessageServices;
import com.wwzz.services.utils.Constants;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 *
 * @author JBOSS
 */
@Service
public class MessageReceiver implements MessageListener,Constants{
    private static final Logger logger=LoggerFactory.getLogger(MessageReceiver.class);
    private MessageServices services;

    public void setServices(MessageServices services) {
        this.services = services;
    }
    
    @Override
    public void onMessage(Message msg) {
        logger.info("Recieved jms message..:",msg);
        if(msg instanceof MapMessage){
            MapMessage messge=(MapMessage) msg;
            int type;
            String notice;
            try {
                type=messge.getInt("type");
                switch(type){
                    case MESSAGE_TYPE_BOOKING:
                        notice=messge.getString("notice");
                        services.process_booking_message(notice);
                        break;
                    case MESSAGE_TYPE_JRS:
                        notice=messge.getString("notice");
                        String bbid=messge.getString("bbid");
                        services.journey_state_notice(notice, bbid);
                        break;
                    case MESSAGE_TYPE_BOOKING_CANCEL:
                        notice=messge.getString("notice");
                        services.booking_cancel_notice(notice);
                        break;
                    case MESSAGE_TYPE_USER_SKILL:
                        notice=messge.getString("notice");
                        services.user_skill_notice(notice);
                        break;
                    case MESSAGE_TYPE_BID_NOTICE:
                        notice=messge.getString("notice");
                        services.bid_notice(notice);
                        break;
                    case MESSAGE_TYPE_NO_BIDDER_BOOKING:
                        notice=messge.getString("notice");
                        services.notice_cancel_no_bidder_booking(notice);
                        break;
                    case MESSAGE_TYPE_BOOKING_PAY_STATE:
                        notice=messge.getString("notice");
                        services.notice_booking_pay_state(notice);
                        break;
                    case MESSAGE_TYPE_PAY_SUCCESS_STATE:
                        notice=messge.getString("notice");
                        break;
                }
                
            } catch (JMSException ex) {
                
            }
        }
    }
    
}
